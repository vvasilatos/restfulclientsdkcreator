package sdk.scan;

/**
 * Created by vasilis vasilatos on 19/2/2016.
 */
public class ResourceObject
{
    String uri;
    String api;

    public String getUri()
    {
        return uri;
    }

    public void setUri( String uri )
    {
        this.uri = uri;
    }

    public String getApi()
    {
        return api;
    }

    public void setApi( String api )
    {
        this.api = api;
    }
}
