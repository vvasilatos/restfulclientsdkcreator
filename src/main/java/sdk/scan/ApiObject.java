package sdk.scan;

/**
 * Created by vasilis vasilatos on 19/2/2016.
 */
public class ApiObject
{
    String protocol;
    String domain;
    String port;
    String service;
    String version;

    public String getProtocol()
    {
        return protocol;
    }

    public void setProtocol( String protocol )
    {
        this.protocol = protocol;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain( String domain )
    {
        this.domain = domain;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort( String port )
    {
        this.port = port;
    }

    public String getService()
    {
        return service;
    }

    public void setService( String service )
    {
        this.service = service;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion( String version )
    {
        this.version = version;
    }

}
