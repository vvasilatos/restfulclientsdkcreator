package sdk.scan;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by vasilis vasilatos on 16/2/2016.
 */
@Target( ElementType.TYPE )
@Retention( RetentionPolicy.RUNTIME )
public @interface Api
{
    String protocol() default "";
    String domain() default "";
    String port() default "";
    String service() default "";
    String version() default "";
}
