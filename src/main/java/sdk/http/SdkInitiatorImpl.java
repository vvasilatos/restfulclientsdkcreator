package sdk.http;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import sdk.scan.Api;
import sdk.scan.ApiObject;
import sdk.scan.Resource;
import sdk.scan.ResourceObject;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by vasilis vasilatos on 18/3/2016.
 */
public class SdkInitiatorImpl
{
    private static SdkInitiatorImpl instance;
    private SdkInitiatorImpl(){}
    static{
        try{
            instance = new SdkInitiatorImpl();
        }catch(Exception e){
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    static SdkInitiatorImpl getInstance(){
        return instance;
    }

    void initLists() throws IOException
    {
        SdkBuilder.getInstance().resources = new HashMap<String, ResourceObject>(  );
        SdkBuilder.getInstance().apis = new HashMap<String, ApiObject>(  );
        try
        {
            File file = new File( SdkBuilder.getInstance().jarToParse);
            URL url = file.toURI().toURL();
            URL[] urls = new URL[]{url};
            ClassLoader cl = new URLClassLoader(urls);
            Reflections reflections = new Reflections(new ConfigurationBuilder().setUrls( ClasspathHelper.forClassLoader(cl)).addClassLoader( cl ));
            Set<Class<?>> annotated = fillApis( reflections );
            annotated.clear();
            fillResources( reflections );
            System.out.println( "-------------->" + SdkBuilder.getInstance().apis.size() );
            System.out.println( "-------------->" + SdkBuilder.getInstance().resources.size() );
        }catch( MalformedURLException e )
        {
            System.out.println(e.getMessage());
        }
    }

    void initializeSdk() throws IOException
    {
        Reflections reflections = new Reflections(  );
        SdkBuilder.getInstance().resources = new HashMap<String, ResourceObject>(  );
        SdkBuilder.getInstance().apis = new HashMap<String, ApiObject>(  );
        Set<Class<?>> annotated = fillApis( reflections );
        annotated.clear();
        fillResources( reflections );
    }

    private void fillResources( Reflections reflections )
    {
        Set<Class<?>> annotated;
        Iterator<Class<?>> iterator;
        annotated = reflections.getTypesAnnotatedWith( Resource.class );
        System.out.println(annotated.size());
        iterator = annotated.iterator();
        System.out.println(iterator);
        while( iterator.hasNext() )
        {
            Class<?> resource = iterator.next();
            System.out.println(resource);
            ResourceObject resourceObject = new ResourceObject();
            Class resourceClass =  resource.getAnnotations()[0].getClass();
            try
            {
                Class noparams[] = {};
                Method method = resourceClass.getMethod( "api", noparams );
                InvocationHandler innerProxy = Proxy.getInvocationHandler(resource.getAnnotations()[0]);
                resourceObject.setApi( (String) innerProxy.invoke(resource.getAnnotations()[0],method,null ) );
                method = resourceClass.getMethod( "uri", noparams );
                resourceObject.setUri( (String) innerProxy.invoke(resource.getAnnotations()[0],method,null ) );
                SdkBuilder.getInstance().resources.put( resource.getSimpleName(), resourceObject );

            }
            catch( NoSuchMethodException e )
            {
                System.out.println(e.getMessage());
            }
            catch( InvocationTargetException e )
            {
                System.out.println(e.getMessage());
            }
            catch( IllegalAccessException e )
            {
                System.out.println(e.getMessage());
            }
            catch( InstantiationException e )
            {
                System.out.println(e.getMessage());
            }
            catch( Throwable throwable )
            {
                System.out.println(throwable.getMessage());
            }

        }
    }

    private Set<Class<?>> fillApis( Reflections reflections )
    {
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith( Api.class );
        System.out.println(annotated.size());
        Iterator<Class<?>> iterator = annotated.iterator();
        while( iterator.hasNext() )
        {

            Class<?> api = iterator.next();
            System.out.println(api.getSimpleName());
            System.out.println(api.getDeclaredAnnotations().length);
            for(Annotation anno:api.getAnnotations())
                System.out.println(anno.getClass());
            ApiObject apiObject = new ApiObject();
            Class apiClass =  api.getAnnotations()[0].getClass();
            try
            {
                Class noparams[] = {};
                Method method = apiClass.getMethod( "protocol", noparams );
                InvocationHandler innerProxy = Proxy.getInvocationHandler(api.getAnnotations()[0]);
                apiObject.setProtocol( (String) innerProxy.invoke(api.getAnnotations()[0],method,null ) );
                method = apiClass.getMethod( "domain", noparams );
                apiObject.setDomain( (String) innerProxy.invoke(api.getAnnotations()[0],method,null ) );
                method = apiClass.getMethod( "port", noparams );
                apiObject.setPort( (String) innerProxy.invoke(api.getAnnotations()[0],method,null ) );
                method = apiClass.getMethod( "service", noparams );
                apiObject.setService( (String) innerProxy.invoke(api.getAnnotations()[0],method,null ) );
                method = apiClass.getMethod( "version", noparams );
                apiObject.setVersion( (String) innerProxy.invoke(api.getAnnotations()[0],method,null ) );
                SdkBuilder.getInstance().apis.put( api.getSimpleName(), apiObject );
            }
            catch( NoSuchMethodException e )
            {
                System.out.println(e.getMessage());
            }
            catch( InvocationTargetException e )
            {
                System.out.println(e.getMessage());
            }
            catch( IllegalAccessException e )
            {
                System.out.println(e.getMessage());
            }
            catch( InstantiationException e )
            {
                System.out.println(e.getMessage());
            }
            catch( Throwable throwable )
            {
                System.out.println(throwable.getMessage());
            }
        }
        return annotated;
    }
}
