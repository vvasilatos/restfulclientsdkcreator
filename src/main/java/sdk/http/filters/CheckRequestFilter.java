package sdk.http.filters;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.Response;

/**
 * Created by vasilis vasilatos on 28/3/2016.
 */
public class CheckRequestFilter implements ClientRequestFilter
{
    public void filter( ClientRequestContext requestContext )
    {
        if(requestContext.getUri().getHost()== null){
            System.out.println("null");
            requestContext.abortWith( Response.status( Response.Status.BAD_REQUEST ).entity( "Error while trying to reach @Api. Host header must be defined. Please check that the 'domain' value is set when using the @Api annotation." ).build() );
            System.out.println("Error while trying to reach @Api. Host header must be defined. Please check that the \'domain\' value is set when using the @Api annotation.");
        }
    }
}
