package sdk.http;

import sdk.scan.ApiObject;
import sdk.scan.ResourceObject;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by vasilis vasilatos on 16/2/2016.
 */

public class SdkBuilder
{

    String targetDirectory;
    String jarToParse;
    HashMap<String, ResourceObject> resources;
    HashMap<String, ApiObject> apis ;

    private static SdkBuilder instance;
    private SdkBuilder(){}
    static{
        try{
            instance = new SdkBuilder();
        }catch(Exception e){
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    public static SdkBuilder getInstance(){
        return instance;
    }

    public static void load(){
        try
        {
            getInstance().initializeSdk();
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }

    public void initializeSdk() throws IOException
    {
        SdkInitiatorImpl.getInstance().initializeSdk();
    }

    void initPlugin(String targetDirectory, String jarToParse) throws IOException
    {
        this.targetDirectory = targetDirectory;
        this.jarToParse = jarToParse;
        SdkInitiatorImpl.getInstance().initLists();
        for(ResourceObject resourceObject:resources.values())
            JsonWriterImpl.getInstance().writeJsonResources( resourceObject );
    }

    public static Object getAll(Class<?> c) throws IOException
    {
        return HttpGetImpl.getInstance().getAll( c );

    }

    public static Object getById(Class<?> parentClass,long id, Class<?> resourceClass) throws IOException
    {
        return HttpGetImpl.getInstance().getById( parentClass, id, resourceClass );
    }
}
