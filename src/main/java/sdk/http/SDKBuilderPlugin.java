package sdk.http;

/**
 * Created by vasilis vasilatos on 24/2/2016.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.IOException;

/**
 * This goal will say a message.
 *
 * @goal sdkBuilder
 * @phase generate-sources
 */
public class SDKBuilderPlugin
    extends AbstractMojo
{
    /**
     * @parameter expression="${sdkCreator.targetDirectory}"
     */
    private String targetDirectory;
    /**
     * @parameter expression="${sdkCreator.jarToParse}"
     */
    private String jarToParse;
    public void execute() throws MojoExecutionException
    {

        try
        {
            SdkBuilder.getInstance().initPlugin(targetDirectory, jarToParse);
        }
        catch( IOException e )
        {
            System.out.println(e.getMessage());
        }

    }
}
