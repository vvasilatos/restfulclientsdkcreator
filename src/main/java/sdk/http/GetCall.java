package sdk.http;

import org.glassfish.jersey.client.ClientConfig;
import sdk.http.filters.CheckRequestFilter;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;

/**
 * Created by vasilis vasilatos on 4/2/2016.
 */
public class GetCall
{
    ClientConfig clientConfig;
    {
        clientConfig = new ClientConfig(  );
        clientConfig.register( CheckRequestFilter.class );
    }
    public String simpleJaxGetCall(String url){
        Client client = ClientBuilder.newClient(clientConfig);
        WebTarget webTarget = client.target( url );
        Invocation.Builder builder = webTarget.request( javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
        Invocation invocation = builder.header( "Accept", javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE).buildGet();
        return invocation.invoke().readEntity( String.class );
    }
    public int simpleJaxGetCallStatus(String url){
        Client client = ClientBuilder.newClient(clientConfig);
        WebTarget webTarget = client.target( url );
        Invocation.Builder builder = webTarget.request( javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE);
        Invocation invocation = builder.header( "Accept", javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE).buildGet();
        return invocation.invoke().getStatus();
    }
}
