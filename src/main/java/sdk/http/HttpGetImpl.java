package sdk.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import sdk.scan.ApiObject;
import sdk.scan.ResourceObject;

import java.io.IOException;

/**
 * Created by vasilis vasilatos on 18/3/2016.
 */
public class HttpGetImpl
{
    private static HttpGetImpl instance;
    private HttpGetImpl(){}
    static{
        try{
            instance = new HttpGetImpl();
        }catch(Exception e){
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    static HttpGetImpl getInstance(){
        return instance;
    }

    Object getAll( Class<?> c ) throws IOException
    {
        ResourceObject resource = SdkBuilder.getInstance().resources.get( c.getSimpleName());
        ApiObject api = SdkBuilder.getInstance().apis.get( resource.getApi() );
        StringBuilder uri = new StringBuilder(  );
        uri.append(api.getProtocol()).append( "://" )
            .append( api.getDomain() );
        if(!api.getPort().equals( "" ))
            uri.append( ":" ).append( api.getPort() );
        if(!api.getService().equals( "" ))
            uri.append( "/" ).append( api.getService() );
        if(!api.getVersion().equals( "" ))
            uri.append( "/" ).append( api.getVersion() );

        uri.append( "/" ).append( resource.getUri() );
        StringBuilder stringBuilder = new StringBuilder( new GetCall().simpleJaxGetCall( uri.toString() ) );
        if(stringBuilder.toString().startsWith( "[" ))
        {
            stringBuilder.insert( 0, "{\"" + c.getSimpleName().toLowerCase() + "\":" );
            stringBuilder.append( "}" );
        }
            return new ObjectMapper().readValue( stringBuilder.toString() , c );
    }

    Object getById( Class<?> parentClass, long id, Class<?> resourceClass ) throws IOException
    {
        ResourceObject resource = SdkBuilder.getInstance().resources.get( parentClass.getSimpleName());
        ApiObject api = SdkBuilder.getInstance().apis.get( resource.getApi() );
        StringBuilder uri = new StringBuilder(  );
        uri.append(api.getProtocol()).append( "://" )
            .append( api.getDomain() );
        if(!api.getPort().equals( "" ))
            uri.append( ":" ).append( api.getPort() );
        if(!api.getService().equals( "" ))
            uri.append( "/" ).append( api.getService() );
        if(!api.getVersion().equals( "" ))
            uri.append( "/" ).append( api.getVersion() );

        uri.append( "/" ).append( resource.getUri() ).append( "/" ).append( String.valueOf( id ) );
        StringBuilder stringBuilder = new StringBuilder( new GetCall().simpleJaxGetCall( uri.toString() ) );
        if(stringBuilder.toString().startsWith( "[" ))
        {
            stringBuilder.insert( 0, "{\"" + resourceClass.getSimpleName().toLowerCase() + "\":" );
            stringBuilder.append( "}" );
        }
        return new ObjectMapper().readValue( stringBuilder.toString() , resourceClass );
    }
}
