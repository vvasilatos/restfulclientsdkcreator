package sdk.http;

import sdk.scan.ApiObject;
import sdk.scan.ResourceObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by vasilis vasilatos on 18/3/2016.
 */
public class JsonWriterImpl
{
    private static JsonWriterImpl instance;
    private JsonWriterImpl(){}
    static{
        try{
            instance = new JsonWriterImpl();
        }catch(Exception e){
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    static JsonWriterImpl getInstance(){
        return instance;
    }

    void writeJsonResources( ResourceObject resourceObject )
    {
        File file = new File( SdkBuilder.getInstance().targetDirectory );
        File json = new File( SdkBuilder.getInstance().targetDirectory + "\\" + resourceObject.getUri());

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.mkdirs();
        }
        if (!json.exists())
        {
            try
            {
                json.createNewFile();
            }
            catch( IOException e )
            {
                System.out.println(e.getMessage());
            }
        }

        FileWriter fw = null;
        try
        {
            fw = new FileWriter(json.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            String response = getJson(resourceObject);
            if(response.startsWith( "[" ))
            {
                StringBuilder stringBuilder = new StringBuilder( response );
                stringBuilder.insert( 0,"{\"" + resourceObject.getUri() + "\":" );
                stringBuilder.append( "}" );
                bw.write(stringBuilder.toString());
            }else{
                bw.write(response);
            }

            bw.close();
        }
        catch( IOException e )
        {
            System.out.println(e.getMessage());
        }
    }

    private String getJson(ResourceObject resourceObject)
    {
        ApiObject api = SdkBuilder.getInstance().apis.get( resourceObject.getApi() );
        StringBuilder uri = new StringBuilder(  );
        uri.append(api.getProtocol()).append( "://" )
            .append( api.getDomain() );
        if(!api.getPort().equals( "" ))
            uri.append( ":" ).append( api.getPort() );
        if(!api.getService().equals( "" ))
            uri.append( "/" ).append( api.getService() );
        if(!api.getVersion().equals( "" ))
            uri.append( "/" ).append( api.getVersion() );

        uri.append( "/" ).append( resourceObject.getUri() );
        return new GetCall().simpleJaxGetCall( uri.toString() );
    }
}
