# Sdk Builder for RESTful Clients
Description: This maven plugin aims to offer an easy way to create a sdk that includes all the POJOs and the basic Http calls to start developing a client for any Restful Api by just giving the minimum information needed (URIs of the Api and Resources that are going to be received from Api) using annotations.

*version:* v0.2

*git repo:* [bitbucket](https://bitbucket.org/vvasilatos/restfulclientsdkcreator/src)
***

## Install the Sdk Builder to your .m2

Download the Sdk Builder project and run:
~~~
mvn clean install
~~~
Now the sdk Builder plugin is in your local .m2 repo.


## Create your sdk
In a new maven project add the Sdk Builder dependency in your pom file:
~~~
    <dependencies>
        <dependency>
            <groupId>sdkBuilderForRestfulClients</groupId>
            <artifactId>sdkBuilderForRestfulClients</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
    </dependencies>
~~~
 You will also need to add these resources and three plugins in the build part of your pom file. Details of how these three plugins work will follow. Have in mind that in Linux or Mac the paths should use "/". In our example we are using Windows paths:
~~~
	<build>
        <resources>
            <resource>
                <directory>${project.build.directory}/java-gen</directory>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>attached</goal>
                        </goals>
                        <phase>process-classes</phase>
                        <configuration>
                            <descriptorRefs>
                                <descriptorRef>jar-with-dependencies</descriptorRef>
                            </descriptorRefs>
                            <outputDirectory>${basedir}\out\artifacts\sdk_jar\</outputDirectory>
                            <finalName>${sdk.name}</finalName>
                            <appendAssemblyId>false</appendAssemblyId>
                        </configuration>
                    </execution>
                    <execution>
                        <id>preparePackage</id>
                        <goals>
                            <goal>assembly</goal>
                        </goals>
                        <phase>prepare-package</phase>
                        <configuration>
                            <descriptorRefs>
                                <descriptorRef>jar-with-dependencies</descriptorRef>
                            </descriptorRefs>
                            <outputDirectory>${project.build.directory}</outputDirectory>
                            <finalName>${sdk.name}</finalName>
                            <appendAssemblyId>false</appendAssemblyId>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>sdkBuilderForRestfulClients</artifactId>
                <groupId>sdkBuilderForRestfulClients</groupId>
                <version>1.0-SNAPSHOT</version>
                <configuration>
                    <targetDirectory>${basedir}\src\json</targetDirectory>
                    <jarToParse>${basedir}\out\artifacts\sdk_jar\${sdk.name}.jar</jarToParse>
                </configuration>
                <executions>
                    <execution>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>sdkBuilder</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.jsonschema2pojo</groupId>
                <artifactId>jsonschema2pojo-maven-plugin</artifactId>
                <version>0.4.19</version>
                <configuration>
                    <sourceType>json</sourceType>
                    <sourceDirectory>${basedir}\src\json</sourceDirectory>
                    <targetPackage>core</targetPackage>
                </configuration>
                <executions>
                    <execution>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
~~~

You will also need to set two properties:
~~~
    <properties>
        <sdk.name>${project.artifactId}-${project.version}</sdk.name>
        <sdk.with.dependencies.name>${sdk.name}-jar-with-dependencies.jar</sdk.with.dependencies.name>
    </properties>
~~~

## Pass the information about the Api.
The information about the Api is passed through @ Api annotation. So by creating a new class and using @ Api annotation you can pass the information about protocol, domain, port, service, version. The default values are "". So in case that the target api has no version just don't add it in your annotation.
~~~
@Api( protocol = "http", domain = "jsonplaceholder.typicode.com", port = "80", service = "", version = "")
public class TargetApi
{
}
~~~

## Pass the information about the Resources.
For each resource of the Api that you want to reach you have to create a class and annotate it using the @ Resource. In this example we need to get the Posts and the Comments from jsonplaceholder.typicode.com:
~~~
@Resource(uri = "posts", api = "TargetApi")
public class Posts
{
}
~~~
~~~
@Resource(uri = "comments", api = "TargetApi")
public class Comments
{
}
~~~

The uri is the string that will actually be added to the relative api in order to fetch the Resource. The api is actually the @ Api that this resource will be connected to. So in this example the resource Posts will be found in the TargetApi in the /posts uri. The final uri will be:
http://jsonplaceholder.typicode.com/posts

## Install your final SDK to your local .m2
Now if you run:
~~~
mvn clean install
~~~

Firstly the maven-assembly-plugin will build a jar for your project. The sdkCreator plugin will scan this jar for @Api and @Resource annotation. After this it will make the required calls for each Resource and it will write the json responses to files inside the /src/json folder. This folder will be used by the jsonschema2pojo-maven-plugin as input to automatically create the POJOs. You can find more details for this project in https://github.com/joelittlejohn/jsonschema2pojo/wiki/Getting-Started#wiki-the-maven-plugin. Finally your maven project is going to be installed in your local .m2

# Use your final SDK
Now that you have your SDK jar file in your local repo you can create your maven project which is going to be the client of the TargetApi. Firstly you will have to add your final SDK to the pom file as a dependency:
~~~
<dependencies>
        <dependency>
            <groupId>yourFinalSdk</groupId>
            <artifactId>yourFinalSdk</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
    </dependencies>
~~~

In your Main method you will have to load the Sdk Builder:
~~~
SdkBuilder.load();
~~~
And after this you can use the auto generated POJOs and the basic calls. In our example, in order to print the Titles of all the Posts and the emails of all the Comments:
~~~
public class Main
{
    public static void main(String[] args)
        throws ClassNotFoundException, NoSuchMethodException, IOException, IllegalAccessException,
        InvocationTargetException
    {

        SdkBuilder.load();

        core.Posts posts = (core.Posts) SdkBuilder.getAll( core.Posts.class );

        for( Post post:posts.getPosts())
            System.out.println(post.getTitle());

        System.out.println("--------------->");

        core.Comments comments = (core.Comments) SdkBuilder.getAll( core.Comments.class );
        for( Comment comment:comments.getComments() )
            System.out.println(comment.getEmail());

        System.out.println("--------------->");

        Post post = (Post) SdkBuilder.getById( Posts.class,100, Post.class );
        System.out.println(post.getTitle());

    }
}
~~~